package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math"
	"reflect"
	"strconv"
	"strings"
)

// Structure for reading from JSON, storing an array of shapes
type Shapes struct{
	Shapes []Shape `json:"shapes"`
}

// Structure for reading from JSON, representing a shape
// 		Name - Shape type
// 		SideA - length of the first side
// 		SideB - length of the second side
// 		Radius - length of the radius
type Shape struct{
	Name   string      `json:"name"`
	SideA  interface{} `json:"side-a,omitempty"`
	SideB  interface{} `json:"side-b,omitempty"`
	Radius interface{} `json:"radius,omitempty"`
}

// Interface for shapes:
// 		Area() - method that allows to get the area of the shape
type IShape interface{
	Area() float64
}

// Prints information about shape area
func printArea(shape IShape, shapeName string){
	capitalizedShapeName := strings.ToUpper(string(shapeName[0])) + shapeName[1:]
	fmt.Printf("%s area is %.3f\n", capitalizedShapeName, shape.Area())
}

// -------------- Square --------------
// Initialized a Square struct:
// 		Name - Shape type
// 		SideA - length of the side
type Square struct{
	Name string `json:"type"`
	SideA float64 `json:",omitempty"`
}

// Returns the shape area
func (square Square) Area() float64{
	return math.Pow(square.SideA, 2)
}

// -------------- Rectangle --------------
// Initialized a Rectangle struct:
// 		Name - Shape type
// 		SideA - length of the first side
// 		SideB - length of the second side
type Rectangle struct{
	Name string `json:"name"`
	SideA float64 `json:",omitempty"`
	SideB float64 `json:",omitempty"`
}

// Returns the shape area
func (rectangle Rectangle) Area() float64 {
	return rectangle.SideA*rectangle.SideB
}

// -------------- Circle --------------
// Initialized a Rectangle struct:
// 		Name - Shape type
// 		Radius - radius of the circle
type Circle struct{
	Name string `json:"name"`
	Radius float64 `json:",omitempty"`
}

// Returns the shape area
func (circle Circle) Area() float64{
	return math.Pi*math.Pow(circle.Radius, 2)
}

// Converts any type of numeric value to float64
func ConvertToFloat64(value interface{}) float64 {
	reflectType := reflect.TypeOf(value)
	reflectValue := reflect.ValueOf(value)

	switch reflectType.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return float64(reflectValue.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return float64(reflectValue.Uint())
	case reflect.Float32, reflect.Float64:
		return reflectValue.Float()
	case reflect.String:
		val, err := strconv.ParseFloat(reflectValue.String(), 64)
		if err != nil {
			panic(err.Error())
		}
		return val
	default:
		panic(errors.New("Incorrect data provided!"))
	}
}

func main() {
	data, _ := ioutil.ReadFile("shapes.json")

	var shapes Shapes
	json.Unmarshal([]byte(data), &shapes)

	for _, shape := range shapes.Shapes {
		var ShapeObj IShape

		switch shape.Name {
		case "square":
			ShapeObj = Square{
				Name:  shape.Name,
				SideA: ConvertToFloat64(shape.SideA),
			}
		case "rectangle":
			ShapeObj = Rectangle{
				Name:  shape.Name,
				SideA: ConvertToFloat64(shape.SideA),
				SideB: ConvertToFloat64(shape.SideB),
			}
		case "circle":
			ShapeObj = Circle{
				Name:   shape.Name,
				Radius: ConvertToFloat64(shape.Radius),
			}
		}

		// shapeArea := ShapeObj.Area() // We can also save the value of the shape's area in the variable
		// fmt.Println(shapeArea)

		printArea(ShapeObj, shape.Name)

		// С использованием printArea() небольшая хитрость.
		// Так как ShapeObj является интерфейсом,
		// я не могу обратиться к полям структур,
		// которые являются наследниками этого интерфейса ShapeObj.Name
		//
		// С помощью ShapeObj.Name не получится обратиться к полю Name структур Square, Rectangle, Circle.
		// Поэтому я воспользовался shape.Name; Вне цикла такое не прокатило бы :(
	}
}